# asset.crusoe.ai flask app 
## defunct
this is no longer in development, keeping for reference JIC

[#jmeyer](https://crusoeenergy.slack.com/team/jmeyer)

[slack thread that inspired this](https://crusoeenergy.slack.com/archives/C02KNTVCMTR/p1675183763339609)

## Purpose of thie PoC

`https://asset.crusoe.ai/` is a prefix in the QR data of our miner asset tags.
This url previously did not exist, and it introduced difficulty to the miner/field team if they were using 3rd party scanners, such as native iphone/android cameras.

The tag data would have to be manually extracted from the 404 error page in the browser or from the returned string from a scanner app.

This is meant to be a super simple service that will return the tag id to the user, and provide a copy button to copy it to text. 

This is a dummy-QR that points to [https://asset.crusoe.ai/10](https://asset.crusoe.ai/10)
![10tag](doc_img/10.png)

Following that url might look something like this:
[https://asset.crusoe.ai/10](https://asset.crusoe.ai/10)
![phone](doc_img/phone.png)

As an added benefit, these URLs can be stored in memory on the server for pasting into a spreadsheet or any other validation purposes.

[https://asset.crusoe.ai/inventory](https://asset.crusoe.ai/inventory)
![phone](doc_img/web.png)

In this example case, there were two bogus tags scanned before the example QR above.  Re-scans or refreshes have no effect on the order or duplicates in the list. 

## Usage

Your phone must be on the tailscale network and have the appropriate ACL to access the tailnet address.
You will also need to accept or acknowledge that the certificate is invalid (only once).

Any smart-phone native QR reader should now be able to visit links via camera.  

Example:
[https://asset.crusoe.ai/312311](https://asset.crusoe.ai/312311)

The tag from the url is simply echo'd back to the browser and logged.  Tags must be 1-6 decimal digits long (regex).

The server will keep track of a unique ordered list of the last tags visited.  
This list will be purged after pressing the purge button or server reset.

## Setup and Installation

This is a super simple PoC python/flask app.  It runs as root to handle :443.

The domain crusoe.ai is in aws.  I have created this record and pointed it to the tailnet address of a utility host I am running in gcp:island-prod.

### Self signed cert

```
openssl req -x509 -newkey rsa:4096 -nodes -out cert.pem -keyout key.pem -days 365
```
the two files created above will be ingested by the flask app.

`run.sh` or `server.py` can be turned into a systemctl service if this provides value.

### Security Risks

There should be minimal security risks running this micro-service. 
The public DNS record will redirect to an internal tailnet address and access to the web-server will require authorization on Tailscale through the user's phone or designated device. 
Additionally, a specific ACL allowance on port 443 must be granted. The service will be run on a non-critical server, most likely a NUC located in the Williston warehouse.
