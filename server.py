#!/usr/bin/env python3

from flask import Flask, render_template, redirect
from werkzeug.routing import BaseConverter
from ordered_set import OrderedSet

app = Flask(__name__)

tags = OrderedSet([])

class RegexConverter(BaseConverter):
    def __init__(self, url_map, *items):
        super(RegexConverter, self).__init__(url_map)
        self.regex = items[0]

app.url_map.converters['regex'] = RegexConverter
app.config['TEMPLATES_AUTO_RELOAD'] = True

@app.route('/<regex("[0-9]{1,6}"):crusoe_id>', methods=["GET"])
def asset_tag_url(crusoe_id):
    tags.add(crusoe_id)
    return render_template('tag.html', tag=crusoe_id)

@app.route('/inventory', methods=["GET"])
def inventory():
    return render_template('inv.html', tags=tags)

@app.route('/purge_inventory', methods=["GET"])
def purge_inventory():
    tags.clear()
    return redirect("/inventory", code=302)

@app.route("/", methods=["GET"])
def hello():
    return render_template('index.html')


if __name__ == "__main__":
    app.run(ssl_context=('cert.pem', 'key.pem'), host='0.0.0.0', port=443)